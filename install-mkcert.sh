#!/bin/bash

MKCERT_VERSION='v1.4.3'

# Colors.
NC='\033[0m' # No Color.
LIGHT_GREEN='\033[1;32m'
RED='\033[0;31m'

declare -A osInfo;
osInfo[/etc/arch-release]=pacman
osInfo[/etc/debian_version]=apt

# Os Detection.
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  echo -e "${LIGHT_GREEN}OS detected: Linux.${NC}"
  BINARY=mkcert-${MKCERT_VERSION}-linux-amd64

  for f in ${!osInfo[@]}
  do
    if [[ -f $f ]]; then
      PM=${osInfo[$f]}
    fi
  done

  if [[ -z $PM ]]; then
    echo "Package manager not identified"
    exit 1;
  fi

  if [[ "$PM" == "apt" ]]; then
    echo -e "${LIGHT_GREEN}Update apt repositories.${NC}"
    sudo apt-get update

    echo -e "${LIGHT_GREEN}Install required dependencies.${NC}"
    sudo apt install wget libnss3-tools -y
  fi

  if [[ "$PM" == "pacman" ]]; then
    echo -e "${LIGHT_GREEN}Install required dependencies.${NC}"
    sudo pacman -Sy --noconfirm wget nss 
  fi

elif [[ "$OSTYPE" == "darwin"* ]]; then
  echo -e "${LIGHT_GREEN}OS detected: Mac OSX.${NC}"
  BINARY=mkcert-${MKCERT_VERSION}-darwin-amd64
else
  echo -e "${Unsupported}OS detected unsupported: ${OSTYPE}.${NC}"
  exit 1;
fi

echo -e "${LIGHT_GREEN}Download mkcert version ${MKCERT_VERSION}.${NC}"
curl -sL -o mkcert https://github.com/FiloSottile/mkcert/releases/download/${MKCERT_VERSION}/${BINARY}

echo -e "${LIGHT_GREEN}Make mkcert executable.${NC}"
chmod +x  mkcert

echo -e "${LIGHT_GREEN}Move mkcert into /usr/local/bin.${NC}"
sudo mv mkcert /usr/local/bin

echo -e "${LIGHT_GREEN}Install certificate authority to avoid validation error.${NC}"
mkcert -install
